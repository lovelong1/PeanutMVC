  <div id='msg-box'>
    <div class='logo'><div>&nbsp;</div></div>
    <div class='title'>您访问的页面发生错误!</div>
    <div class='msg'></div>
	<#if errmessage?exists><div class='errmsg'><b>错误内容：</b>${errmessage}</div></#if>
    <div class='nav'><a href="${ContextRoot}/">返回首页</a>&nbsp;&nbsp;<a href="javascript:history.go(-1)">返回上页</a></div>
  </div>
