package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.mvc.admin.model.User;

public interface RightManager extends Manager<Right> {
	/**
	 * 获得分页
	 * @param pageNo 当前页数
	 * @param pageSize 每页显示数
	 * @return 分页数据
	 */
	public PageModel<Right> findAll(IMyDb db,int pageNo, int pageSize,String groupid,String rightnum,String rightname,Boolean iuse) throws SQLException, WKDbException;
	
	/**
	 * 获得Right 数据
	 * @param conn 
	 * @return Right 列表
	 */
	public List<Right> findAll(IMyDb db,String groupid,String rightnum,String rightname,Boolean iuse) throws SQLException, WKDbException;
	
	/**
	 * 根据权限编码和权限名称查询 Right
	 * @param conn
	 * @param rightnum
	 * @param rightname
	 * @return Right
	 * @throws Exception
	 */
	public Right findByNumName(IMyDb db,String groupid,String rightnum,String rightname) throws SQLException, WKDbException;
	
	/**
	 * 根据用户得到权限列表
	 * @param conn
	 * @param user
	 * @return
	 * @throws SQLException 
	 */
	public List<Right> findByUser(IMyDb db,User user) throws SQLException, WKDbException;
}