package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Rightgroup;



public class RightgroupManagerImp extends ManagerImp<Rightgroup> implements RightgroupManager {
	
	private static final Log loger = LogFactory.getLog(RightgroupManagerImp.class);
	public IDbFactory dbFactory = new DbFactory();
	
	public RightgroupManagerImp() {
		loger.debug("begin RightGroup ManagerImp");
	}
	
	public Class<?> getModuleClass(){
		return Rightgroup.class;
	}
	
	public List<Rightgroup> findAll(IMyDb db,String pid) throws SQLException, WKDbException  {
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT * from tb_sus_rightgroup WHERE 1=1 ");
		if(StringUtils.isEmpty(pid)){
			sqlUtil.setSQL("AND (parentid is NULL OR parentid=?) ");
			sqlUtil.addParams(0);
		}else{
			sqlUtil.put("parentid", pid);
		}
		sqlUtil.setSQL("order by groupid");
		return dbFactory.query(db, sqlUtil.getSQL(), Rightgroup.class,sqlUtil.getParams().toArray());
	}

	public int modifyparentid(IMyDb db,Rightgroup rightgroup,String oldparentid) throws SQLException, WKDbException {
		String queryString = "UPDATE tb_sus_rightgroup set parentid=? where parentid= ?";
		return dbFactory.execute(db, queryString,Rightgroup.class, rightgroup.getParentid(),oldparentid);
	}
	
	public List<JSONObject> gettree(IMyDb db,String pid) throws JSONException, SQLException, WKDbException {
		List<Rightgroup> list = findAll(db,pid);
		List<JSONObject> lists = new ArrayList<JSONObject>();
		for (Rightgroup rightgroup2 : list) {
			JSONObject orgObject = new JSONObject();
			orgObject.put("id", rightgroup2.getGroupid());
			orgObject.put("text", rightgroup2.getGroupname());
			orgObject.put("value", rightgroup2.getGroupid());
			orgObject.put("isexpand", false);
			orgObject.put("checkstate", 0);
			List<Rightgroup> list2 = findAll(db,rightgroup2.getGroupid());
			if (list2.size() > 0) {
				orgObject.put("hasChildren", true);
				orgObject.put("showcheck", false);
			} else {
				orgObject.put("hasChildren", false);
				orgObject.put("showcheck", true);
			}
			orgObject.put("ChildNodes", null);
			orgObject.put("complete", false);
			orgObject.put("pid", rightgroup2.getParentid());
			lists.add(orgObject);
		}
		return lists;
	}
	
	
	
	public List<Rightgroup> getall(IMyDb db,Integer num,String parentid) throws JSONException, SQLException, WKDbException {
		String strv = "";
		for (int i = 0; i < num; i++) {
			strv = strv+"　";
		}
		List<Rightgroup> rightgroups = new ArrayList<Rightgroup>();
		List<Rightgroup> rightgroups2 = findAll(db, parentid);
		for (Rightgroup rightgroup : rightgroups2) {
			rightgroup.setGroupname(strv+rightgroup.getGroupname());
			rightgroups.add(rightgroup);
			List<Rightgroup> rightgroups3 = getall(db, (num+1),rightgroup.getGroupid());
			for (Rightgroup rightgroup2 : rightgroups3) {
				rightgroups.add(rightgroup2);
			}
		}
		return rightgroups;
	}
	
}
