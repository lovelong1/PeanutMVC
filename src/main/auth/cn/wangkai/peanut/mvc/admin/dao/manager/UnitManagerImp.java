package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.bean.auth.LoginUser;
import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.mvc.admin.model.Unit;
import cn.wangkai.peanut.util.RequestContext;

public class UnitManagerImp extends ManagerImp<Unit> implements UnitManager {

	private static final Log loger = LogFactory.getLog(UnitManagerImp.class);
	public IDbFactory dbFactory = new DbFactory();
	public UnitManagerImp() {
		loger.debug("Begin UnitManagerImp");
	}
	
	public Class<?> getModuleClass(){
		return Unit.class;
	}

	public int modifysjdwbh(IMyDb db,Unit unit,String olddwbh)  throws SQLException, WKDbException{
		String queryString = "UPDATE tb_sus_Unit set dwbh=? where dwbh= ?";
		return dbFactory.execute(db, queryString,unit.getDwbh(),olddwbh);
	}

	public PageModel<Unit> findAll(IMyDb db,String dwxh,Integer sfbm,int pageNo, int pageSize)
			throws SQLException, WKDbException {
		PageModel<Unit> pageModel = null;
		ArrayList<Object> params = new ArrayList<Object>();
		StringBuffer SQL = new StringBuffer("from tb_sus_Unit WHERE 1=1 ");
		StringBuffer SQLWHERE = new StringBuffer();
		if (SQLWHERE.length() > 0) {
			SQL.append(SQLWHERE);
		}
		pageModel = dbFactory.get_cutpage(db, Unit.class, "",
				SQL.toString(), pageSize, pageNo, params, Unit.PRIMARYKEY, true);

		return pageModel;
	}

	public List<Unit> findAll(IMyDb db,String dwxh,Integer sfbm) throws SQLException, WKDbException {
		RequestContext context = RequestContext.get();
		LoginUser user =  context.getUser();
		if(user==null) throw new SQLException("用户未登录");
		ArrayList<Object> Objectlist = new ArrayList<Object>();
		StringBuffer SQL = new StringBuffer("SELECT * from tb_sus_Unit WHERE 1=1 ");
		StringBuffer SQLWHERE = new StringBuffer();
		if(StringUtils.isNotEmpty(dwxh)){
			SQLWHERE.append(" AND sjdw=? ");
			Objectlist.add(dwxh);
		}else{
			SQLWHERE.append(" AND sjdw is NULL OR sjdw = ? ");
			Objectlist.add(0);
		}
		if(!user.cr("SYS_ADMIN")){
/**			if(user.getXzqhs()==null||user.getXzqhs().length<=0){
				return null;
			}
			String[] xzqhs =user.getXzqhs();
			for (String xzqh : xzqhs) {
				Objectlist.add(xzqh);
			}
			SQLWHERE.append(" AND xzqhdm in ("+SQLUtil.putlistsql(xzqhs.length)+") ");
			**/
			SQLWHERE.append(" AND dwbh like ? ");
			Objectlist.add(user.getUnit().getDwbh()+"%");
		}
		if(sfbm!=null){
			SQLWHERE.append(" AND sfbm=? ");
			Objectlist.add(sfbm);
		}
		SQL.append(SQLWHERE);
		SQL.append(" order by sfbm,dwbh ");
		return dbFactory.query(db, SQL.toString(), Unit.class,Objectlist.toArray());
	}

	public Unit findById(IMyDb db,String unitid) throws SQLException, WKDbException {
		Unit unit = null;
		String queryString = "SELECT * from tb_sus_Unit where dwxh= ?";
		unit = (Unit) dbFactory.getOne(db, queryString,Unit.class, unitid);
		return unit;
	}
	
	public Unit findBydwbh(IMyDb db,String dwbh) throws SQLException, WKDbException{
		Unit unit = null;
		String queryString = "SELECT * from tb_sus_Unit where dwbh= ?";
		unit = (Unit) dbFactory.getOne(db, queryString,Unit.class, dwbh);
		return unit;
	}

	
	public List<Unit> getall(IMyDb db,Integer num,String dwxh,Integer sfbm) throws SQLException, WKDbException{
		String strv = "";
		for (int i = 0; i < num; i++) {
			strv = strv+"　";
		}
		List<Unit> units = new ArrayList<Unit>();
		List<Unit> units2 = findAll(db, dwxh, sfbm);
		for (Unit unit : units2) {
			unit.setDwmc(strv+unit.getDwmc());
			unit.setDwjc(strv+unit.getDwjc());
			units.add(unit);
			List<Unit> units3 = getall(db, (num+1),unit.getDwxh(), sfbm);
			for (Unit unit2 : units3) {
//				unit2.setDwmc(strv+"　"+unit2.getDwmc());
//				unit2.setDwjc(strv+"　"+unit2.getDwjc());
				units.add(unit2);
			}
		}
		return units;
	}

}
