package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.util.List;

import com.alibaba.fastjson.JSONArray;

import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.mvc.admin.model.MenuRight;
import cn.wangkai.peanut.mvc.admin.model.Navmenu;
import cn.wangkai.peanut.mvc.admin.model.Right;


public interface MenurightManager extends Manager<MenuRight> {
	/**
	 * 
	 * @param menuid Menu 
	 * @param conn 
	 * @return Menu 
	 */
	public MenuRight findById(IMyDb db,String sid) throws Exception ;
	
	public List<Right> findAll(IMyDb db,String menuid) throws Exception;
	
	public MenuRight findBymenuid(IMyDb db,String menuid,String rightid) throws Exception;
	
	public void del(IMyDb db,String menuid,String rightids) throws Exception;
	
	public JSONArray getMenu(IMyDb db,String userid) throws Exception;
	
	public List<Navmenu> getuser(IMyDb db,String userid) throws Exception;
	
	public void destroy();
}