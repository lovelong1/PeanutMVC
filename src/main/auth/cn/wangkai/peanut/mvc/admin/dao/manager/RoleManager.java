package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.mvc.admin.model.Role;
import cn.wangkai.peanut.mvc.admin.model.User;

public interface RoleManager extends Manager<Role> {

	/**
	 * 获得分页
	 * @param pageNo 当前页数
	 * @param pageSize 每页显示数
	 * @return 分页数据
	 */
	public PageModel<Role> findAll(IMyDb db,int pageNo, int pageSize,String groupid)  throws SQLException, WKDbException ;
	
	/**
	 * 获得Role 数据
	 * @param conn 
	 * @return Role 列表
	 */
	public List<Role> findAll(IMyDb db,String groupid)  throws SQLException, WKDbException ;
	
	/**
	 * 根据角色名称查询Role
	 * @param conn
	 * @param rolename
	 * @return Role
	 * @ throws SQLException, WKDbException
	 */
	public Role findByRolename(IMyDb db,String rolename)  throws SQLException, WKDbException;

	/**
	 * 根据用户得到角色列表
	 * @param conn
	 * @param user
	 * @return
	 * @throws SQLException 
	 */
	public List<Role> findByUser(IMyDb db,User user) throws SQLException, WKDbException;
}