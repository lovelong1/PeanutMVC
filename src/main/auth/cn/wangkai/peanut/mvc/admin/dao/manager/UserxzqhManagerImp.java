package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Userxzqh;



public class UserxzqhManagerImp extends ManagerImp<Userxzqh> implements UserxzqhManager {
	private static  Log log = LogFactory.getLog(UserxzqhManagerImp.class);
	public IDbFactory dbFactory = new DbFactory();
	
	public UserxzqhManagerImp() {
	}
	
	public Class<?> getModuleClass(){
		return Userxzqh.class;
	}
	
	public void modify(IMyDb db,String userid,String xzqhdms) throws Exception{
		log.debug(userid);
		log.debug(xzqhdms);
		if(StringUtils.isEmpty(userid)) throw new WKDbException("用户数据不存在！");
/**		SQLUtil delsql = new SQLUtil("DELETE FROM TB_SUS_USERXZQH WHERE USERID=? ",userid);
		if(StringUtils.isNotBlank(xzqhdms)){
			if(xzqhdms.indexOf(",")>0){
				String[] xzqhdm = xzqhdms.split(",");
				delsql.setSQL("AND xzqhdm NOT IN ("+SQLUtil.putlistsql(xzqhdm.length)+") ");
				for (String xzqhdm2 : xzqhdm) {
					delsql.addParams(xzqhdm2);
				}
			}else{
				delsql.putstr("xzqhdm", xzqhdms);
			}
		}
		dbFactory.execute(db, delsql.getSQL(), delsql.getParams().toArray());**/
		String[] strs = find(db, userid);
		Map<String, String> xzqhmap = new HashMap<String, String>();
		if(strs!=null){
			for (String str : strs) {
				xzqhmap.put(str, str);
			}
		}
		/**
		if(StringUtils.isNotBlank(xzqhdms)){
			if(xzqhdms.indexOf(",")>0){
				String[] xzqhdm = xzqhdms.split(",");
				for (String xzqhdm2 : xzqhdm) {
					if(xzqhmap.get(xzqhdm2)==null) add(db, userid, xzqhdm2);
				}
			}else{
				if(xzqhmap.get(xzqhdms)==null) add(db, userid, xzqhdms);
			}
		}**/
	}
	
	/**
	private void add(IMyDb db,String userid,String xzqhdm) throws ManagerException, WKDbException, SQLException{
		Userxzqh userxzqh =new Userxzqh();
		userxzqh.setUserid(userid);
		userxzqh.setXzqhdm(xzqhdm);
		dbFactory.add(db, userxzqh);
	}***/
	
	public String[] find(IMyDb db,String userid) throws Exception{
		SQLUtil sqlUtil = new SQLUtil("SELECT USERID, XZQHDM FROM TB_SUS_USERXZQH WHERE USERID=? ",userid);
		List<Userxzqh> userxzqhs = dbFactory.query(db, Userxzqh.class, sqlUtil);
		List<String> xzqhdms = new ArrayList<String>();
		for (Userxzqh userxzqh : userxzqhs) {
			xzqhdms.add(userxzqh.getXzqhdm());
		}
		if(xzqhdms.size()<=0) return null;
		return xzqhdms.toArray(new String[xzqhdms.size()]);
	}
	
	public String find2Str(IMyDb db,String userid) throws Exception{
		String[] xzqhdms = find(db, userid);
		if(xzqhdms==null) return null;
		String xzqhdmstr = null;
		for (String xzqhdm : xzqhdms) {
			if(xzqhdmstr==null)
				xzqhdmstr = xzqhdm;
			else
				xzqhdmstr += ","+xzqhdm;
		}
		return xzqhdmstr;
	}
}
