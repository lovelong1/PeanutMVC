package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.bean.auth.LoginUser;
import cn.wangkai.peanut.cache.Cache;
import cn.wangkai.peanut.cache.WebCacheManager;
import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.MyDb;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.ManagerException;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Msg;
import cn.wangkai.peanut.mvc.admin.model.User;
import cn.wangkai.peanut.system.db.PeanutDb;



public class MsgManagerImp extends ManagerImp<Msg> implements MsgManager {
	
	private static final Log loger = LogFactory.getLog(MsgManagerImp.class);
	private IDbFactory dbFactory = new DbFactory();
	private PeanutDb peanutDb = PeanutDb.Open();
	private static Cache cache;
	public Class<?> getModuleClass(){
		return Msg.class;
	}
	
	public MsgManagerImp() {
		loger.debug("Begin cache for msg");
		if(cache==null)	cache = WebCacheManager.getCache("msg", true);
	}

	public void destroy(){
		WebCacheManager.stop();
	}


	public PageModel<Msg> findAll(IMyDb db,String p,String searchstr,LoginUser loginUser,int pageNo, int pageSize)  throws SQLException, WKDbException, ManagerException, WKDbException  {
		SQLUtil sqlUtil = new SQLUtil("FROM tb_sus_msgs WHERE lyzr=? ",loginUser.getUser().getUserid());
		if(p==null||"new".equalsIgnoreCase(p)){
		}else if("user".equalsIgnoreCase(p)){
			sqlUtil.putstr("lylx", Msg.USERMSG);
		}else if("sys".equalsIgnoreCase(p)||"system".equalsIgnoreCase(p)){
			sqlUtil.putstr("lylx", Msg.SYSMSG);
		}else if("my".equalsIgnoreCase(p)){
			sqlUtil.putstr("lyfsz", loginUser.getUser().getUserid());
		}
		sqlUtil.putlike("lynr", searchstr);
		
		return dbFactory.get_cutpage(db, Msg.class, "",sqlUtil, pageSize, pageNo, Msg.PRIMARYKEY, true);
	}
	
	public void updatests(IMyDb db, List<Msg> lists) throws SQLException, WKDbException, ManagerException, WKDbException {
		if(lists==null||lists.size()<=0) return;
			
		List<Long> ids = new ArrayList<Long>();
		for (Msg msg : lists) {
			if(Msg.ZT_NEW.equalsIgnoreCase(msg.getZt())) ids.add(msg.getLyxh());
		}
		if(ids.size()>0){
			SQLUtil sqlUtil = new SQLUtil("UPDATE "+Msg.TABLENAME+" SET ZT = ? ",Msg.ZT_READ);
			if(ids.size()==1){
				sqlUtil.setSQL("WHERE lyxh=?");
				sqlUtil.addParams(ids.get(0));
			}else{
				sqlUtil.setSQL("WHERE lyxh in ("+SQLUtil.putlistsql(ids.size())+")");
				for (Long id : ids) {
					sqlUtil.addParams(id);
				}
				
			}
			dbFactory.execute(db,sqlUtil.getSQL(), sqlUtil.getParams().toArray());
		}
	}
	
	public void clearnewmsg(User user){
		String key = user.getUserid()+"_new_msg";
		if(cache.get(key)!=null) cache.remove(key);
	}
	
	public int findnewmsg(User user){
		IMyDb db = MyDb.Open(peanutDb);
		String key = user.getUserid()+"_new_msg";
		if(cache.get(key)!=null) return cache.get(key);
		try{
			SQLUtil sqlUtil = new SQLUtil("SELECT COUNT(*) AS TJ FROM tb_sus_msgs WHERE lyzr=? AND ZT=? ",user.getUserid(),"1");
			BigDecimal num = dbFactory.getcount(db, sqlUtil);
			cache.put(key, num==null?0:num.intValue());
			return num==null?0:num.intValue();
		}catch (Exception e) {
			return 0;
		}finally{
			db.Close();
		}
		
	}
	
}
