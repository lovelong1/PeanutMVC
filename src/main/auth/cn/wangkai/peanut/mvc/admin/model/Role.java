package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Role 
 * 2011-12-26 19:57:31 
 *  
 * @version 1.00 
 */ 
public class Role implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_role";
	public static final String PRIMARYKEY="roleid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
	private String roleid;
	private String groupid;
	private String rolename;

	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

}