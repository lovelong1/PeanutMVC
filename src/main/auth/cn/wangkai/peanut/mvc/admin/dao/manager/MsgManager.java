package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.bean.auth.LoginUser;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.db.util.ManagerException;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.mvc.admin.model.Msg;
import cn.wangkai.peanut.mvc.admin.model.User;

public interface MsgManager extends Manager<Msg> {

	/**
	 * 获得分页
	 * @param pageNo 当前页数
	 * @param pageSize 每页显示数
	 * @return 分页数据
	 */
	public PageModel<Msg> findAll(IMyDb db,String p,String searchstr,LoginUser loginUser,int pageNo, int pageSize) throws SQLException, WKDbException, ManagerException, WKDbException ;
	public void clearnewmsg(User user);
	public int findnewmsg(User user);
	public void destroy();
	public void updatests(IMyDb db, List<Msg> lists) throws Exception;
}