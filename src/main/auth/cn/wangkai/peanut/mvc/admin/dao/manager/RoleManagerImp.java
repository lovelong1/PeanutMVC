package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDb;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Role;
import cn.wangkai.peanut.mvc.admin.model.User;



public class RoleManagerImp extends ManagerImp<Role> implements RoleManager {
	
	private static final Log loger = LogFactory.getLog(RoleManagerImp.class);
	public IDbFactory dbFactory = new DbFactory();
	
	public RoleManagerImp() {
		loger.debug("Begin RoleManagerImp");
	}
	
	public Class<?> getModuleClass(){
		return Role.class;
	}

	public PageModel<Role> findAll(IMyDb db,int pageNo, int pageSize,String groupid)   throws SQLException, WKDbException {
		PageModel<Role> pageModel = null;
		ArrayList<Object> params = new ArrayList<Object>();
		//SELECT * FROM tb_sus_role a 
		//LEFT JOIN tb_sus_rightgroup b on a.GROUPID=b.GROUPID
		StringBuffer SQL = new StringBuffer("from tb_sus_role a LEFT JOIN tb_sus_rightgroup b on a.GROUPID=b.GROUPID WHERE 1=1 ");
		StringBuffer SQLWHERE = new StringBuffer();
		if(StringUtils.isNotEmpty(groupid)){
			SQLWHERE.append(" AND a.groupid=? ");
			params.add(groupid);
		}else{
			SQLWHERE.append(" AND a.groupid is NULL ");
		}
		if (SQLWHERE.length() > 0) {
			SQL.append(SQLWHERE);
		}
		pageModel = dbFactory.get_cutpage(db, Role.class, "a.*,b.groupname ",SQL.toString(), pageSize, pageNo, params, Role.PRIMARYKEY, true);
		return pageModel;
	}
	
	public List<Role> findAll(IMyDb db,String groupid)  throws SQLException, WKDbException {
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT a.ROLEID,a.GROUPID,");
		if(db.getDb().getDbtype()==IDb.DB_MYSQL){
			sqlUtil.setSQL("CONCAT(b.GROUPNAME,' ',a.ROLENAME) as ROLENAME ");
		}else if(db.getDb().getDbtype()==IDb.DB_INTPLE||db.getDb().getDbtype()==IDb.DB_ORACLE){
			sqlUtil.setSQL("b.GROUPNAME||' '||a.ROLENAME as ROLENAME ");
		}else{
			sqlUtil.setSQL("b.GROUPNAME+' '+a.ROLENAME as ROLENAME ");
		}
		sqlUtil.setSQL("from tb_sus_role a ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_rightgroup b on a.GROUPID=b.GROUPID  ");
		sqlUtil.setSQL("WHERE 1=1 ");
		if(groupid==null){
			sqlUtil.setSQL("AND a.groupid is NULL OR a.groupid=? ");
			sqlUtil.addParams(0);
		}else{
			sqlUtil.put("a.groupid", groupid);
		}
		sqlUtil.setSQL("order by a.roleid");
		return dbFactory.query(db, sqlUtil.getSQL(), Role.class,sqlUtil.getParams().toArray());
	}
	
	public Role findByRolename(IMyDb db,String rolename)  throws SQLException, WKDbException {
		Role role = null;
		String queryString = "SELECT * from tb_sus_role where rolename = ?";
		role = (Role) dbFactory.getOne(db, queryString,Role.class, rolename);
		return role;
	}
	public List<Role> findByUser(IMyDb db,User user) throws SQLException, WKDbException{
		ArrayList<Object> params = new ArrayList<Object>();
		StringBuffer SQL = new StringBuffer("SELECT * FROM tb_sus_role ");
		SQL.append("WHERE roleid in (");
		SQL.append("SELECT ROLEID FROM tb_sus_userroleright ");
		SQL.append("WHERE userid=? GROUP BY ROLEID)");
		params.add(user.getUserid());
		return dbFactory.query(db, SQL.toString(), Role.class,params.toArray());
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

	}
}
