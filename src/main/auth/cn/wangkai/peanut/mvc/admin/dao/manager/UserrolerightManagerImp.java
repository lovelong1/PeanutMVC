package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDb;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.mvc.admin.model.Role;
import cn.wangkai.peanut.mvc.admin.model.Userroleright;



public class UserrolerightManagerImp extends ManagerImp<Userroleright> implements UserrolerightManager {
	private static  Log loger = LogFactory.getLog(UserrolerightManagerImp.class);
	public IDbFactory dbFactory = new DbFactory();
	
	public UserrolerightManagerImp() {
		loger.debug("");
	}
	public Class<?> getModuleClass(){
		return Userroleright.class;
	}
	
	public List<Right> findRight(IMyDb db,String userid)  throws SQLException, WKDbException{
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT b.RIGHTID,b.RIGHTNUM,b.GROUPID,");
		if(db.getDb().getDbtype()==IDb.DB_MYSQL){
			sqlUtil.setSQL("CONCAT(c.GROUPNAME,' ',b.RIGHTNUM,' ',b.RIGHTNAME) as RIGHTNAME ");
		}else if(db.getDb().getDbtype()==IDb.DB_ORACLE||db.getDb().getDbtype()==IDb.DB_INTPLE){
			sqlUtil.setSQL("c.GROUPNAME||' '||b.RIGHTNUM||' '||b.RIGHTNAME as RIGHTNAME ");
		}else{
			sqlUtil.setSQL("c.GROUPNAME+' '+b.RIGHTNUM+' '+b.RIGHTNAME as RIGHTNAME ");
		}
		sqlUtil.setSQL(",b.IUSE FROM tb_sus_userroleright a ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_right b on a.RIGHTID=b.RIGHTID ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_rightgroup c on b.GROUPID=c.GROUPID ");
		sqlUtil.setSQL("WHERE 1=1 AND a.ROLEID IS NULL ");
		sqlUtil.put("a.USERID", userid);
		sqlUtil.setSQL("order by a.sid");
		return dbFactory.query(db, sqlUtil.getSQL(), Right.class,sqlUtil.getParams().toArray());
	}
	
	public List<Right> findallRight(IMyDb db,String userid)  throws SQLException, WKDbException{
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT b.* FROM tb_sus_userroleright a ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_right b on a.RIGHTID=b.RIGHTID WHERE 1=1 ");
		sqlUtil.put("a.USERID", userid);
		sqlUtil.setSQL("order by sid");
		return dbFactory.query(db,sqlUtil.getSQL(), Right.class,sqlUtil.getParams().toArray());
	}
	
	public List<Role> findRole(IMyDb db,String userid)  throws SQLException, WKDbException{
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT b.ROLEID,b.GROUPID,");
		if(db.getDb().getDbtype()==IDb.DB_MYSQL){
			sqlUtil.setSQL("CONCAT(c.GROUPNAME,' ',b.ROLENAME) as ROLENAME ");
		}else if(db.getDb().getDbtype()==IDb.DB_ORACLE||db.getDb().getDbtype()==IDb.DB_INTPLE){
			sqlUtil.setSQL("c.GROUPNAME||' '||b.ROLENAME as ROLENAME ");
		}else{
			sqlUtil.setSQL("c.GROUPNAME+' '+b.ROLENAME as ROLENAME ");
		}
		
		sqlUtil.setSQL(" FROM tb_sus_userroleright a ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_role b on a.ROLEID=b.ROLEID ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_rightgroup c on b.GROUPID=c.GROUPID ");
		sqlUtil.setSQL("WHERE 1=1 AND a.ROLEID IS NOT NULL ");
		sqlUtil.put("a.USERID", userid);
		sqlUtil.setSQL("group by b.ROLEID,b.GROUPID,b.ROLENAME,c.GROUPNAME order by b.ROLEID");
		return dbFactory.query(db,sqlUtil.getSQL(), Role.class,sqlUtil.getParams().toArray());
	}
	
	public Userroleright findById(IMyDb db,String sid)   throws SQLException, WKDbException {
		return dbFactory.getOne(db, "SELECT * from tb_sus_userroleright where sid= ?",Userroleright.class, sid);
	}
	
	public Userroleright findByuserid(IMyDb db, String userid,String rightid, String Roleid) throws SQLException, WKDbException{
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT * from tb_sus_userroleright where 1=1 ");
		sqlUtil.put("USERID", userid);
		sqlUtil.put("RIGHTID", rightid);
		if(Roleid==null)
			sqlUtil.setSQL("AND ROLEID is NULL ");
		else
			sqlUtil.put("ROLEID", Roleid);
		sqlUtil.setSQL("order by ROLEID");
		return dbFactory.getOne(db, sqlUtil.getSQL(),Userroleright.class, sqlUtil.getParams().toArray());
		
	}
	
	public void delright(IMyDb db,String userid,String rightids)  throws SQLException, WKDbException{
		String querystring = "DELETE FROM tb_sus_userroleright WHERE ROLEID IS NULL AND USERID=?";
		if(StringUtils.isNotBlank(rightids)){
			querystring = querystring + " AND RIGHTID not in ("+rightids+")";
		}
		dbFactory.execute(db, querystring, userid);
	}
	
	public void delrole(IMyDb db,String userid,String roles)  throws SQLException, WKDbException{
		String querystring = "DELETE FROM tb_sus_userroleright WHERE ROLEID IS NOT NULL AND USERID=?";
		if(StringUtils.isNotBlank(roles)){
			querystring = querystring + " AND ROLEID not in ("+roles+")";
		}
		dbFactory.execute(db, querystring, userid);
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

	}
}
