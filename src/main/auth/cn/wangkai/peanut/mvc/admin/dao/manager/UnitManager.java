package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.mvc.admin.model.Unit;

public interface UnitManager extends Manager<Unit> {

	public int modifysjdwbh(IMyDb db,Unit unit,String olddwbh)  throws SQLException, WKDbException ;
	/**
	 * 获得分页
	 * @param pageNo 当前页数
	 * @param pageSize 每页显示数
	 * @return 分页数据
	 */
	public PageModel<Unit> findAll(IMyDb db,String dwxh,Integer sfbm,int pageNo, int pageSize) throws SQLException, WKDbException ;
	
	/**
	 * 获得Unit 数据
	 * @return Unit 列表
	 */
	public List<Unit> findAll(IMyDb db,String dwxh,Integer sfbm) throws SQLException, WKDbException;
	
	/**
	 * 
	 * @param Unit Unit 
	 * @return Unit 
	 */
	public Unit findById(IMyDb db,String dwxh) throws SQLException, WKDbException;
	
	
	public Unit findBydwbh(IMyDb db,String dwbh) throws SQLException, WKDbException;
	
	public List<Unit> getall(IMyDb db,Integer num,String dwxh,Integer sfbm) throws SQLException, WKDbException;
}