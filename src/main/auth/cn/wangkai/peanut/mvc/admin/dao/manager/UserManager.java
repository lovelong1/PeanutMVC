package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.mvc.admin.model.User;

public interface UserManager extends Manager<User> {
	/**
	 * 更新密码
	 * @param conn
	 * @param user
	 * @throws Exception
	 */
	public void updatepassword(IMyDb db,User user) throws Exception;

	/**
	 * 获得分页
	 * @param conn
	 * @param pageNo 当前页数
	 * @param pageSize 每页显示数
	 * @param unitid
	 * @return 分页数据
	 */
	public PageModel<Object> findAll(IMyDb db,int pageNo, int pageSize,String unitid,String username,String loginid,Boolean isuse) throws SQLException, WKDbException;

	/**
	 * @param conn
	 * @param Userid 
	 * @return User 
	 */
	public User findByUserName(IMyDb db,String Userid) throws SQLException, WKDbException;
	public User findByLoginuser(IMyDb db,String Loginid) throws SQLException, WKDbException;
	
	/**
	 * 查询所有用户
	 */
	public List<User> findAllUser(IMyDb db) throws SQLException, WKDbException;
	
	
	public List<User> findAllUsername(IMyDb db) throws SQLException, WKDbException;
	
	
	//CACHE
	
	public User findUserByUserID(IMyDb db,String userid) throws SQLException, WKDbException;
	public User findUserByName(IMyDb db,String username) throws SQLException, WKDbException;
	public void destroy();
}