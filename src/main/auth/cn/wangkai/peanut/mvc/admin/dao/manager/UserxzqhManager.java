package cn.wangkai.peanut.mvc.admin.dao.manager;

import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.mvc.admin.model.Userxzqh;

public interface UserxzqhManager extends Manager<Userxzqh> {

	public void modify(IMyDb db,String userid,String xzqhdms) throws Exception ;
	
	public String[] find(IMyDb db,String userid) throws Exception ;
	
	public String find2Str(IMyDb db,String userid) throws Exception ;
}