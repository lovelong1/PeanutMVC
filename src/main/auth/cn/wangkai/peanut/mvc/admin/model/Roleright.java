package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Roleright 
 * 2011-12-26 19:57:31 
 *  
 * @version 1.00 
 */ 
public class Roleright implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_roleright";
	public static final String PRIMARYKEY="sid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
	private String sid;
	private String roleid;
	private String rightid;

	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getRightid() {
		return rightid;
	}
	public void setRightid(String rightid) {
		this.rightid = rightid;
	}

}