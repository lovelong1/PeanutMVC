package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Menu 
 * 2011-12-26 19:57:31 
 *
 * @version 1.00 
 */ 
public class MenuRight implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_menuright";
	public static final String PRIMARYKEY="sid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
	private String sid;
	private String menuid;
	private String rightid;

	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getMenuid() {
		return menuid;
	}
	public void setMenuid(String menuid) {
		this.menuid = menuid;
	}
	public String getRightid() {
		return rightid;
	}
	public void setRightid(String rightid) {
		this.rightid = rightid;
	}

}