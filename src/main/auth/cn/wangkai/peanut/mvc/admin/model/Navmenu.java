package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Navmenu 
 * 2011-12-26 19:57:31 
 *
 * @version 1.00 
 */ 
public class Navmenu implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_navmenu";
	public static final String PRIMARYKEY="menuid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
	private String menuid;
	private String menuname;
	private String parentmenuid;
	private String slink;
	private String icon;
	private String nindex;

	public String getMenuid() {
		return menuid;
	}
	public void setMenuid(String menuid) {
		this.menuid = menuid;
	}
	public String getMenuname() {
		return menuname;
	}
	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}
	public String getParentmenuid() {
		return parentmenuid;
	}
	public void setParentmenuid(String parentmenuid) {
		this.parentmenuid = parentmenuid;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getNindex() {
		return nindex;
	}
	public void setNindex(String nindex) {
		this.nindex = nindex;
	}
	public String getSlink() {
		return slink;
	}
	public void setSlink(String slink) {
		this.slink = slink;
	}

}