package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Rightgroup 
 * 2011-12-26 19:57:31 
 *  
 * @version 1.00 
 */ 
public class Rightgroup implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_rightgroup";
	public static final String PRIMARYKEY="groupid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
	private String groupid;
	private String groupname;
	private String parentid;
	private Boolean iuse;

	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public Boolean getIuse() {
		return iuse;
	}
	public void setIuse(Boolean iuse) {
		this.iuse = iuse;
	}

}