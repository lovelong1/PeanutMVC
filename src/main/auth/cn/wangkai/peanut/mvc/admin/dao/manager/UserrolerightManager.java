package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.mvc.admin.model.Role;
import cn.wangkai.peanut.mvc.admin.model.Userroleright;

public interface UserrolerightManager extends Manager<Userroleright> {
	
	public List<Right> findRight(IMyDb db,String userid)  throws SQLException, WKDbException ;
	
	public List<Right> findallRight(IMyDb db,String userid)  throws SQLException, WKDbException ;
	
	public List<Role> findRole(IMyDb db,String userid)  throws SQLException, WKDbException ;

	public Userroleright findByuserid(IMyDb db, String userid,String rightid, String Roleid) throws SQLException, WKDbException;
	
	public void delright(IMyDb db,String userid,String rightids)  throws SQLException, WKDbException;
	
	public void delrole(IMyDb db,String userid,String roles)  throws SQLException, WKDbException;
}