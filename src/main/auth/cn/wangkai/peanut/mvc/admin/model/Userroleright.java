package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Userroleright 
 * 2011-12-26 19:57:31 
 *
 * @version 1.00 
 */ 
public class Userroleright implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_userroleright";
	public static final String PRIMARYKEY="sid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
  	//sid
  	//ColumnTypeName=int
	private String sid;
  	//rightid
  	//ColumnTypeName=int
	private String rightid;
  	//roleid
  	//ColumnTypeName=int
	private String roleid;
  	//userid
  	//ColumnTypeName=int
	private String userid;

	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getRightid() {
		return rightid;
	}
	public void setRightid(String rightid) {
		this.rightid = rightid;
	}
	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

}