package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Right 
 * 2011-12-26 19:57:31 
 *
 * @version 1.00 
 */ 
public class Right implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_right";
	public static final String PRIMARYKEY="rightid";
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;
	private String rightid;
	private String rightnum;
	private String groupid;
	private String rightname;
	private Boolean iuse;

	public String getRightid() {
		return rightid;
	}
	public void setRightid(String rightid) {
		this.rightid = rightid;
	}
	public String getRightnum() {
		return rightnum;
	}
	public void setRightnum(String rightnum) {
		this.rightnum = rightnum;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getRightname() {
		return rightname;
	}
	public void setRightname(String rightname) {
		this.rightname = rightname;
	}
	public Boolean getIuse() {
		return iuse;
	}
	public void setIuse(Boolean iuse) {
		this.iuse = iuse;
	}

}