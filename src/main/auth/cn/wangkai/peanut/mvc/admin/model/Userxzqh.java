package cn.wangkai.peanut.mvc.admin.model;

/** 
 * Userxzqh 
 * 2011-12-26 19:57:31 
 *
 * @version 1.00 
 */ 
public class Userxzqh implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_userxzqh";
	public static final String[] PRIMARYKEY={"userid,xzqhdm"};
	public static final boolean KEYWORDAUTO = false;
	
	private static final long serialVersionUID = 1L;

	//用户序号
	private String userid;
	//行政区划编号
	private String xzqhdm;
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getXzqhdm() {
		return xzqhdm;
	}
	public void setXzqhdm(String xzqhdm) {
		this.xzqhdm = xzqhdm;
	}


}