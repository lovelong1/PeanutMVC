package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDb;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.PageModel;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.mvc.admin.model.User;



public class RightManagerImp extends ManagerImp<Right> implements RightManager {
	
	private static final Log loger = LogFactory.getLog(RightManagerImp.class);
	public IDbFactory dbFactory = new DbFactory();
	
	public RightManagerImp() {
		loger.debug("Begin RightManagerImp");
	}
	public Class<?> getModuleClass(){
		return Right.class;
	}
	public PageModel<Right> findAll(IMyDb db,int pageNo, int pageSize,String groupid,String rightnum,String rightname,Boolean iuse)   throws SQLException, WKDbException {
		PageModel<Right> pageModel = null;
		ArrayList<Object> params = new ArrayList<Object>();
		StringBuffer SQL = new StringBuffer("from tb_sus_right WHERE 1=1 ");
		StringBuffer SQLWHERE = new StringBuffer();
		if(groupid==null){
			groupid=null;
		}
		if(StringUtils.isNotEmpty(groupid)){
			SQLWHERE.append(" AND groupid=? ");
			params.add(groupid);
		}else{
			SQLWHERE.append(" AND groupid is NULL ");
		}
		if(StringUtils.isNotBlank(rightnum)){
			SQLWHERE.append(" AND rightnum like ? ");
			params.add("%"+rightnum+"%");
		}
		if(StringUtils.isNotBlank(rightname)){
			SQLWHERE.append(" AND rightname like ? ");
			params.add("%"+rightname+"%");
		}
		if(iuse!=null){
			SQLWHERE.append(" AND iuse=? ");
			params.add(iuse);
		}
		if (SQLWHERE.length() > 0) {
			SQL.append(SQLWHERE);
		}
		pageModel = dbFactory.get_cutpage(db, Right.class, "",SQL.toString(), pageSize, pageNo, params, Right.PRIMARYKEY, true);

		return pageModel;
	}
	
	public List<Right> findAll(IMyDb db,String groupid,String rightnum,String rightname,Boolean iuse)  throws SQLException, WKDbException {
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT a.RIGHTID,a.RIGHTNUM,a.GROUPID,");
		
		if(db.getDb().getDbtype()==IDb.DB_MYSQL){
			sqlUtil.setSQL("CONCAT(b.GROUPNAME,' ',a.RIGHTNUM,' ',a.RIGHTNAME) as RIGHTNAME");
		}else if(db.getDb().getDbtype()==IDb.DB_ORACLE||db.getDb().getDbtype()==IDb.DB_INTPLE){
			sqlUtil.setSQL("b.GROUPNAME||' '||a.RIGHTNUM||' '||a.RIGHTNAME as RIGHTNAME");
		}else{
			sqlUtil.setSQL("b.GROUPNAME+' '+a.RIGHTNUM+' '+a.RIGHTNAME as RIGHTNAME");
		}
		sqlUtil.setSQL(",a.IUSE FROM tb_sus_right a ");
		sqlUtil.setSQL("LEFT JOIN tb_sus_rightgroup b on a.GROUPID=b.GROUPID ");
		sqlUtil.setSQL("WHERE 1=1 ");

		if(StringUtils.isEmpty(groupid)){
			sqlUtil.setSQL("AND (a.groupid is NULL OR a.groupid=?) ");
			sqlUtil.addParams(0);
		}else{
			sqlUtil.put("a.groupid", groupid);
		}
		sqlUtil.putlike("a.rightnum", rightnum);
		sqlUtil.putlike("a.rightname", rightname);
		sqlUtil.put("a.iuse", iuse);
		sqlUtil.setSQL("order by a.rightid");
		return dbFactory.query(db,sqlUtil.getSQL(), Right.class,sqlUtil.getParams().toArray());
	}

	public Right findByNumName(IMyDb db,String groupid,String rightnum,String rightname)  throws SQLException, WKDbException {
		Right right = null;
		String queryString = "SELECT * from tb_sus_right where groupid = ? and rightnum= ? and rightname=?";
		right = (Right) dbFactory.getOne(db, queryString,Right.class,groupid,rightnum,rightname);
		return right;
	}
	
	public List<Right> findByUser(IMyDb db,User user) throws SQLException, WKDbException{
		SQLUtil sqlUtil = new SQLUtil();
		sqlUtil.setSQL("SELECT * FROM tb_sus_right WHERE RIGHTID in (");
		sqlUtil.setSQL("	SELECT RIGHTID FROM tb_sus_userroleright WHERE userid=? GROUP BY RIGHTID ");
		sqlUtil.addParams(user.getUserid());
		sqlUtil.setSQL("	UNION ");
		sqlUtil.setSQL("	SELECT RIGHTID FROM tb_sus_roleright WHERE roleid in (");
		sqlUtil.setSQL("		SELECT ROLEID FROM tb_sus_userroleright WHERE userid=? GROUP BY ROLEID ");
		sqlUtil.addParams(user.getUserid());
		sqlUtil.setSQL("	) AND iuse=? GROUP BY RIGHTID) ");
		sqlUtil.addParams(true);
		return dbFactory.query(db, sqlUtil.getSQL(), Right.class,sqlUtil.getParams().toArray());
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

	}
}
