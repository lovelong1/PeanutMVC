package cn.wangkai.peanut.mvc.admin.model;

public class Unit implements java.io.Serializable  {
	private static final long serialVersionUID = 577713634458883218L;
	public static final String TABLENAME="tb_sus_unit";
	public static final String PRIMARYKEY="dwxh";
	public static final boolean KEYWORDAUTO = false;
	
	private String dwxh;
	private String dwbh;
	private String dwmc;
	private String dwjc;
	private String sjdw;
    private Integer dwjb;
    private Integer sfbm;
	private String xzqhdm;
	public String getDwxh() {
		return dwxh;
	}
	public void setDwxh(String dwxh) {
		this.dwxh = dwxh;
	}
	public String getDwbh() {
		return dwbh;
	}
	public void setDwbh(String dwbh) {
		this.dwbh = dwbh;
	}
	public String getDwmc() {
		return dwmc;
	}
	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}
	public String getDwjc() {
		return dwjc;
	}
	public void setDwjc(String dwjc) {
		this.dwjc = dwjc;
	}
	public String getSjdw() {
		return sjdw;
	}
	public void setSjdw(String sjdw) {
		this.sjdw = sjdw;
	}
	public Integer getDwjb() {
		return dwjb;
	}
	public void setDwjb(Integer dwjb) {
		this.dwjb = dwjb;
	}
	public Integer getSfbm() {
		return sfbm;
	}
	public void setSfbm(Integer sfbm) {
		this.sfbm = sfbm;
	}
	public String getXzqhdm() {
		return xzqhdm;
	}
	public void setXzqhdm(String xzqhdm) {
		this.xzqhdm = xzqhdm;
	}
	
	
	
}
