package cn.wangkai.peanut.mvc.admin.model;

import java.sql.Timestamp;

public class Msg implements java.io.Serializable {
	
	public static final String TABLENAME="tb_sus_msgs";
	public static final String PRIMARYKEY="lyxh";
	public static final boolean KEYWORDAUTO = false;
	
	public static String SYSMSG = "1";
	public static String USERMSG = "2";
	
	public static String ZT_NEW = "1";
	public static String ZT_READ = "2";
	
	private static final long serialVersionUID = 1L;
	//留言序号
	private Long lyxh;//   number(20)             not null,
	//留言主人
	private String lyzr;//   varchar2(50)           not null,
	//留言对方
	private String lydf;//   varchar2(50)           not null,
	//留言发送者
	private String lyfsz;//  varchar2(50)           not null,
	//留言接收者
	private String lyjsz;//  varchar2(50)           not null,
	//留言类型
	private String lylx;//   char(1)                not null,
	//留言内容
	private String lynr;//   varchar2(4000)         not null,
	//发送日期
	private Timestamp fsrq;//   date                   not null,
	//阅读日期
	private Timestamp ydrq;//   date                   null    ,
	//状态
	private String zt;//     char(1)                not null,
	public Long getLyxh() {
		return lyxh;
	}
	public void setLyxh(Long lyxh) {
		this.lyxh = lyxh;
	}
	public String getLyzr() {
		return lyzr;
	}
	public void setLyzr(String lyzr) {
		this.lyzr = lyzr;
	}
	public String getLydf() {
		return lydf;
	}
	public void setLydf(String lydf) {
		this.lydf = lydf;
	}
	public String getLyfsz() {
		return lyfsz;
	}
	public void setLyfsz(String lyfsz) {
		this.lyfsz = lyfsz;
	}
	public String getLyjsz() {
		return lyjsz;
	}
	public void setLyjsz(String lyjsz) {
		this.lyjsz = lyjsz;
	}
	public String getLylx() {
		return lylx;
	}
	public void setLylx(String lylx) {
		this.lylx = lylx;
	}
	public String getLynr() {
		return lynr;
	}
	public void setLynr(String lynr) {
		this.lynr = lynr;
	}
	public Timestamp getFsrq() {
		return fsrq;
	}
	public void setFsrq(Timestamp fsrq) {
		this.fsrq = fsrq;
	}
	public Timestamp getYdrq() {
		return ydrq;
	}
	public void setYdrq(Timestamp ydrq) {
		this.ydrq = ydrq;
	}
	public String getZt() {
		return zt;
	}
	public void setZt(String zt) {
		this.zt = zt;
	}
	
	
}
