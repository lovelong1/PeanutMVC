package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.mvc.admin.model.Navmenu;

public interface NavmenuManager extends Manager<Navmenu> {
	
	/**
	 * 获得Navmenu 数据
	 * @param conn 
	 * @return Navmenu 列表
	 */
	public List<Navmenu> findAll(IMyDb db,String parentid,String menuidstr) throws SQLException, WKDbException ;
	
	public int modifyparentid(IMyDb db,Navmenu menu ,String oldparentid)  throws SQLException, WKDbException;
	
	/**
	 * 
	 * @param conn
	 * @param pid
	 * @return
	 * @ throws SQLException, WKDbException, ManagerException, WKDbException 
	 */
	public List<JSONObject> gettree(IMyDb db,String pid) throws JSONException, SQLException, WKDbException ;
	
	public List<JSONObject> getusertree(IMyDb db,String pid,String menuidstr)  throws JSONException, SQLException, WKDbException;

}