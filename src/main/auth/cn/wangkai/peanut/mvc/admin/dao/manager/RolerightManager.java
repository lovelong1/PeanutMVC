package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.mvc.admin.model.Roleright;

public interface RolerightManager extends Manager<Roleright> {

	
	/**
	 * 
	 * @param rolerightid Roleright 
	 * @param conn 
	 * @return Roleright 
	 */
	public Roleright findByroleid(IMyDb db,String roleid,String rightid)  throws SQLException, WKDbException;
	
	public void del(IMyDb db,String roleid,String rightids)  throws SQLException, WKDbException;

	public List<Right> findrole(IMyDb db,String roleid)  throws SQLException, WKDbException;
	
}