package cn.wangkai.peanut.mvc.admin.model;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = 4323028740519266221L;
	public static final String TABLENAME="tb_sus_user";
	public static final String PRIMARYKEY="userid";
	public static final boolean KEYWORDAUTO = false;
	
	private String userid;
	private String username;
	private String loginid;
	private String loginpassword;
	private Boolean sex;
	private String unitid;
	private String departmentid;
	private String postid;
	private String idnumber;
	private String tel;
	private String email;
	private String mark;
	private Boolean isuse;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getLoginpassword() {
		return loginpassword;
	}
	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}
	public String getUnitid() {
		return unitid;
	}
	public void setUnitid(String unitid) {
		this.unitid = unitid;
	}
	public String getDepartmentid() {
		return departmentid;
	}
	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}
	public String getPostid() {
		return postid;
	}
	public void setPostid(String postid) {
		this.postid = postid;
	}
	public String getIdnumber() {
		return idnumber;
	}
	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public Boolean isSex() {
		return sex;
	}
	public void setSex(Boolean sex) {
		this.sex = sex;
	}
	public Boolean isIsuse() {
		return isuse;
	}
	public void setIsuse(Boolean isuse) {
		this.isuse = isuse;
	}
	public Boolean getSex() {
		return sex;
	}
	public Boolean getIsuse() {
		return isuse;
	}
	
}
