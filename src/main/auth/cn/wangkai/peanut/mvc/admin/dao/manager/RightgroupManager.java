package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.List;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.Manager;
import cn.wangkai.peanut.mvc.admin.model.Rightgroup;

public interface RightgroupManager extends Manager<Rightgroup> {

	
	public List<Rightgroup> findAll(IMyDb db,String pid) throws SQLException, WKDbException;
	/**
	 * 
	 * @param con Connection
	 * @param rightgroup rightgroup
	 * @param oldparentid oldparentid
	 * @return
	 * @throws Exception
	 */
	public int modifyparentid(IMyDb db,Rightgroup rightgroup,String oldparentid) throws SQLException, WKDbException ;
	
	/**
	 * 
	 * @param conn Connection
	 * @param pid parentid
	 * @return
	 * @throws Exception
	 */
	public List<JSONObject> gettree(IMyDb db,String pid) throws JSONException, SQLException, WKDbException;
	
	/**
	 * 
	 * @param con
	 * @param num
	 * @param parentid
	 * @return
	 * @throws Exception
	 */
	public List<Rightgroup> getall(IMyDb db,Integer num,String parentid) throws JSONException, SQLException, WKDbException;
}