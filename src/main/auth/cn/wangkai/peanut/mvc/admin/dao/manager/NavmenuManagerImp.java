package cn.wangkai.peanut.mvc.admin.dao.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import cn.wangkai.peanut.db.DbFactory;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IDbFactory;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.db.manager.ManagerImp;
import cn.wangkai.peanut.db.util.SQLUtil;
import cn.wangkai.peanut.mvc.admin.model.Navmenu;



public class NavmenuManagerImp extends ManagerImp<Navmenu> implements NavmenuManager {
	
	private static final Log loger = LogFactory.getLog(NavmenuManagerImp.class);
	private IDbFactory dbFactory = new DbFactory();
	
	public NavmenuManagerImp() {
		loger.debug("Begin NavmenuManagerImp");
	}
	public Class<?> getModuleClass(){
		return Navmenu.class;
	}
	public List<Navmenu> findAll(IMyDb db,String parentid,String menuidstr) throws SQLException, WKDbException {
		SQLUtil sql = new SQLUtil();
		sql.setSQL("SELECT * from tb_sus_navmenu WHERE 1=1 ");
		
		if(StringUtils.isNotEmpty(parentid)){
			sql.putstr("parentmenuid", parentid);
		}else{
			sql.putNull("parentmenuid", true);
		}
		if(StringUtils.isNotBlank(menuidstr)){
			sql.setSQL(" AND menuid in ("+menuidstr+") ");
		}
		sql.setSQL("order by nindex,menuid");
		
		return dbFactory.query(db, Navmenu.class, sql);
	}
	
	public int modifyparentid(IMyDb db,Navmenu menu ,String oldparentid) throws SQLException, WKDbException{
		return dbFactory.execute(db, "UPDATE tb_sus_navmenu set parentmenuid=? where parentmenuid= ?",
				Navmenu.class, menu.getParentmenuid(),oldparentid);
	}

	public List<JSONObject> gettree(IMyDb db,String pid)  throws JSONException, SQLException, WKDbException{
		List<Navmenu> list = findAll(db,pid,null);
		List<JSONObject> lists = new ArrayList<JSONObject>();
		for (Navmenu menu : list) {
			JSONObject orgObject = new JSONObject();
			orgObject.put("id", menu.getMenuid());
			orgObject.put("text",menu.getMenuname());
			orgObject.put("value",menu.getMenuid());
			orgObject.put("isexpand", false);
			orgObject.put("checkstate", 0);
			List<Navmenu> list2 = findAll(db,menu.getMenuid(),null);
			if (list2.size() > 0) {
				orgObject.put("hasChildren", true);
				orgObject.put("showcheck", false);
			} else {
				orgObject.put("hasChildren", false);
				orgObject.put("showcheck", true);
			}
			orgObject.put("ChildNodes",null);
			orgObject.put("complete", false);
			orgObject.put("pid",menu.getParentmenuid());
			lists.add(orgObject);
		}
		return lists;
	}
	
	
	public List<JSONObject> getusertree(IMyDb db,String pid,String menuidstr) throws JSONException, SQLException, WKDbException{
		List<Navmenu> list = findAll(db,pid,menuidstr);
		List<JSONObject> lists = new ArrayList<JSONObject>();
		for (Navmenu menu : list) {
			JSONObject orgObject = new JSONObject();
			orgObject.put("id", menu.getMenuid());
			orgObject.put("text",menu.getMenuname());
			orgObject.put("value",menu.getSlink());
			orgObject.put("isexpand", menu.getParentmenuid() == null?true:false);
			orgObject.put("checkstate", 0);
			List<JSONObject> list2 = getusertree(db,menu.getMenuid(),menuidstr);
			if (list2.size() > 0) {
				orgObject.put("hasChildren", true);
				orgObject.put("showcheck", false);
			} else {
				orgObject.put("hasChildren", false);
				orgObject.put("showcheck", true);
			}
			orgObject.put("ChildNodes",list2);
			orgObject.put("complete", true);
			orgObject.put("pid", menu.getParentmenuid() );
			lists.add(orgObject);
		}
		return lists;
	}

}
