package cn.wangkai.peanut.dao.auth;

import java.io.IOException;
import java.net.MalformedURLException;

import com.alibaba.fastjson.JSONArray;

import cn.wangkai.peanut.bean.auth.AccessToken;
import cn.wangkai.peanut.bean.auth.LoginUser;

public interface AuthUserManager {

	/**
	 * 获取与验证服务交互吗
	 * @param client_id 客户端编码 
	 * @return 系统验证信息
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws JSONException 
	 */
	public AccessToken access_token(String client_id) throws AuthManagerException;
	
	/**
	 * 验证用户
	 * @param token 系统验证信息
	 * @param LoginID 登录系统账号
	 * @param Password 登录系统密码
	 * @return 当前登录用户信息
	 * @throws AuthManagerException
	 */
	public LoginUser login(AccessToken token,String LoginID,String Password) throws AuthManagerException;
	
	/**
	 * 得到系统菜单的Json对象
	 * @param token 系统验证信息
	 * @return 当前用户菜单列表
	 * @throws AuthManagerException
	 */
	public JSONArray menu(AccessToken token) throws AuthManagerException;
	
	public boolean loginout(AccessToken token,String LoginID,String Password) throws AuthManagerException;
}
