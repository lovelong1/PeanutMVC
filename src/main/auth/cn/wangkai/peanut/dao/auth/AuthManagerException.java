package cn.wangkai.peanut.dao.auth;

public class AuthManagerException  extends Exception {
    private int statusCode = -1;
    private static final long serialVersionUID = -2623309261327598087L;

    public AuthManagerException(String msg) {
        super(msg);
    }

    public AuthManagerException(Exception cause) {
        super(cause);
    }

    public AuthManagerException(String msg, int statusCode) {
        super(msg);
        this.statusCode = statusCode;

    }

    public AuthManagerException(String msg, Exception cause) {
        super(msg, cause);
    }

    public AuthManagerException(String msg, Exception cause, int statusCode) {
        super(msg, cause);
        this.statusCode = statusCode;

    }

    public int getStatusCode() {
        return this.statusCode;
    }
}
