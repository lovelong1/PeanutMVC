package cn.wangkai.peanut.bean.auth;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;

import cn.wangkai.peanut.dao.auth.AuthManagerException;

public class AccessToken {
	
	public AccessToken(JSONObject $json) throws AuthManagerException {
		this.setSuccees($json.getBoolean("succees"));
		this.setClient_id($json.getString("client_id"));
		this.setAccess_token($json.getString("access_token"));
		this.setAccess_time($json.getLong("access_time"));
	}
	
	private boolean succees;
	private Date access_time;
	private String client_id;
	private String access_token;
//	{
//		"succees":true,
//		"access_time":1345878640140,
//		"client_id":"ff7f9d875016452397b28af9ce71ee63",
//		"access_token":"6b294414a4de40b3b8a45b7c814e20fc"
//	}
	public boolean isSuccees() {
		return succees;
	}
	public void setSuccees(boolean succees) {
		this.succees = succees;
	}
	public Date getAccess_time() {
		return access_time;
	}
	public void setAccess_time(Date access_time) {
		this.access_time = access_time;
	}
	public void setAccess_time(Long access_time) {
		if(access_time!=null&&access_time>0){
			this.access_time = new Date(access_time);
		}
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}


}
