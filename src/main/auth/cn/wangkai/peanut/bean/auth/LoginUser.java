package cn.wangkai.peanut.bean.auth;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.wangkai.peanut.dao.auth.AuthManagerException;
import cn.wangkai.peanut.mvc.admin.dao.manager.MsgManager;
import cn.wangkai.peanut.mvc.admin.dao.manager.MsgManagerImp;
import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.mvc.admin.model.Role;
import cn.wangkai.peanut.mvc.admin.model.Unit;
import cn.wangkai.peanut.mvc.admin.model.User;

public class LoginUser implements Serializable {
	
	private MsgManager msgManager = new MsgManagerImp();
	private static final long serialVersionUID = 4431994886449446099L;
	private static final Log log = LogFactory.getLog(LoginUser.class);

	
	private User user;//用户
    private Unit unit;//部门
    private Unit department;//部门
    private List<Role> roles;
    private List<Right> rights;
    private String[] xzqhs;
    private HashMap<String, Role> qroles;
    private HashMap<String, Right> qrights;
    private HashMap<String, String> qxzqhs;
    
	public LoginUser(JSONObject $json) throws AuthManagerException {
		user = new User();
		department = new Unit();
		unit = new Unit();
		roles = new ArrayList<Role>();
		rights = new ArrayList<Right>();
		qroles = new HashMap<String, Role>();
		qrights = new HashMap<String, Right>();
		try {
			if($json!=null){
				JSONObject $dept = $json.getJSONObject("department");
				JSONObject $unit = $json.getJSONObject("unit");
				user.setUserid($json.getString("userid"));
				user.setUsername($json.getString("username"));
				user.setSex($json.getBoolean("sex"));
				user.setLoginid($json.getString("loginid"));
				user.setPostid($json.getString("postid"));
				user.setIdnumber($json.getString("idnumber"));
				user.setTel($json.getString("tel"));
				user.setEmail($json.getString("email"));
				user.setMark($json.getString("mark"));
				user.setIsuse($json.getBoolean("isuse"));
				//单位
				if($unit!=null){
					user.setUnitid($unit.getString("id"));
					department.setDwxh($unit.getString("id"));
					department.setDwbh($unit.getString("number"));
					department.setDwmc($unit.getString("name"));
					department.setDwjc($unit.getString("shortname"));
					department.setSjdw($unit.getString("parentid"));
					department.setDwjb($unit.getInteger("level"));
					department.setXzqhdm($unit.getString("district"));
					department.setSfbm(0);
				}
				//部门
				if($dept!=null){
					user.setUnitid($dept.getString("id"));
					unit.setDwxh($dept.getString("id"));
					unit.setDwbh($dept.getString("number"));
					unit.setDwmc($dept.getString("name"));
					unit.setSjdw($dept.getString("departmentid"));
//					unit.setDwjc($unit.getString("shortname"));
//					unit.setDwjb($unit.getInt("level"));
//					unit.setXzqhdm($unit.getString("district"));
					unit.setSfbm(1);
				}
				
				JSONArray $rights = $json.getJSONArray("rights");
				for (int i = 0; i < $rights.size(); i++) {
					JSONObject $right = $rights.getJSONObject(i);
					Right right = new Right();
					right.setGroupid($right.getString("groupid"));
					right.setRightid($right.getString("rightid"));
					right.setRightnum($right.getString("rightnum"));
					right.setRightname($right.getString("rightname"));
					right.setIuse(true);
					qrights.put(right.getRightnum(), right);
					rights.add(right);
				}
				
				JSONArray $roles = $json.getJSONArray("roles");
				for (int i = 0; i < $roles.size(); i++) {
					JSONObject $role = $roles.getJSONObject(i);
					Role role = new Role();
					role.setGroupid($role.getString("groupid"));
					role.setRoleid($role.getString("roleid"));
					role.setRolename($role.getString("rolename"));
					qroles.put(role.getRolename(),role);
					roles.add(role);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new AuthManagerException("返回对象无法正常转化！",e);
		}
	}

    
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	public Unit getDepartment() {
		return department;
	}
	public void setDepartment(Unit department) {
		this.department = department;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		qroles = new HashMap<String, Role>();
		for (Role role : roles) {
			qroles.put(role.getRoleid(), role);
		}
		this.roles = roles;
	}
	public List<Right> getRights() {
		return rights;
	}
	public void setRights(List<Right> rights) {
		qrights = new HashMap<String, Right>();
		for (Right right : rights) {
			qrights.put(right.getRightnum(), right);
		}
		this.rights = rights;
	}
	public HashMap<String, Role> getQroles() {
		return qroles;
	}
	public void setQroles(HashMap<String, Role> qroles) {
		this.qroles = qroles;
	}
	public HashMap<String, Right> getQrights() {
		return qrights;
	}
	public void setQrights(HashMap<String, Right> qrights) {
		this.qrights = qrights;
	}
    
	public String[] getXzqhs() {
		return xzqhs;
	}
	public void setXzqhs(String[] xzqhs) {
		qxzqhs = new HashMap<String, String>();
		for (String xzqh : xzqhs) {
			qxzqhs.put(xzqh, xzqh);
		}
		this.xzqhs = xzqhs;
	}
	public HashMap<String, String> getQxzqhs() {
		return qxzqhs;
	}
	public void setQxzqhs(HashMap<String, String> qxzqhs) {
		this.qxzqhs = qxzqhs;
	}
	
	/**
	 * 获得当前用户自定权限，如果没有改权限，返回NULL
	 * @param rightnum
	 * @return
	 */
	public Right getright(String rightnum){
		Right right = qrights.get(rightnum);
		return right;
	}
	public int getMsgs() {
		return msgManager.findnewmsg(user);
	}
	/**
	 * 检查是否具有权限
	 * @param rightnums
	 * @return
	 */
    public boolean cr(String... rightnums){
    	if(qrights==null||qrights.size()==0||rightnums==null) return false;
		for (String rightnum : rightnums) {
			if(qrights.get(rightnum)!=null) return true;
		}
		return false;
    }
}
