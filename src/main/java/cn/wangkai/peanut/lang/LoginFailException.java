package cn.wangkai.peanut.lang;

public class LoginFailException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public LoginFailException() {
		
	}

	public LoginFailException(String message) {
		super(message);
	}

	public LoginFailException(Throwable cause) {
		super(cause);
	}

	public LoginFailException(String message, Throwable cause) {
		super(message, cause);
	}
}
