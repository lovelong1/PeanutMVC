package cn.wangkai.peanut.action.util;

import org.apache.commons.lang.StringUtils;

import cn.wangkai.peanut.bean.auth.LoginUser;
import cn.wangkai.peanut.lang.ActionException;
import cn.wangkai.peanut.lang.LoginFailException;
import cn.wangkai.peanut.util.Configinfo;
import cn.wangkai.peanut.util.RequestContext;


public class LoginCheckAction extends BaseAction  {
	public static String USERSESSIONID = Configinfo.getconfig("USERSESSIONID", "B7FDBDE8-DB9E-454C-BB22-CD41C77A13B2");

	public void checkUser(RequestContext context) throws LoginFailException{
		LoginUser loginUser = (LoginUser) context.session().getAttribute(USERSESSIONID);
		if(loginUser==null||loginUser.getUser()==null||StringUtils.isEmpty(loginUser.getUser().getUserid())) throw new LoginFailException("用户尚未登录!");
	}
	
	public void checkAdminUser(RequestContext context) throws Exception{
		checkUser(context);
	}
	
	public void CheckRight(RequestContext context,String ...rightnum) throws ActionException, LoginFailException{
		checkUser(context);
		if(!context.getUser().cr(rightnum)) throw new ActionException("你无权限操作！");
	}
	
	
	public String getParams(String[] parts,int lv,String defultstr){
		int i = 0;
		for (String string : parts) {
			System.out.println("parts["+i+"]="+string);
			i++;
		}
		if(parts==null||parts.length<=lv) return defultstr;
		return parts[lv];
	}
	
	public String getParams(String[] parts,int lv){
		return getParams(parts, lv, null);
	}
	
}
