package cn.wangkai.peanut.util;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

public class HTMLTools {

	public static String strtoHTML(String content){
//		  content= StringUtils.replace(content,"&","&amp"); 
////		  content= StringUtils.replace(content,"'","''"); 
//		  content= StringUtils.replace(content,"<","&lt"); 
//		  content= StringUtils.replace(content,">","&gt"); 
//		  content= StringUtils.replace(content,"chr(60)","&lt"); 
//		  content= StringUtils.replace(content,"chr(37)","&gt"); 
//		  content= StringUtils.replace(content,"\"","&quot"); 
//		  content= StringUtils.replace(content,";",";"); 
//		  content = StringUtils.replace(content,"/t","&nbsp;&nbsp;&nbsp;&nbsp;");//替换跳格
//		  content= StringUtils.replace(content,"\n","<br/>"); 
//		  content= StringUtils.replace(content," ","&nbsp"); 
//		  return content;
		  return  StringEscapeUtils.escapeHtml(content);
		  
	}
	public static String strtoJs(String content){
//		  content = StringUtils.replace(content,"\"","\\\"");//替换跳格
//		  content = StringUtils.replace(content,"\n","\\\n");//替换跳格
//		return content;
		return  StringEscapeUtils.escapeJavaScript(content);
	}
	
	
	public static String strtojsstr(String str){
			str = StringUtils.replace(str, "\"", "\\\"");
			str = StringUtils.replace(str, "\n", "\\\n");
			str = StringUtils.replace(str, "\t", "\\\n");
			return str;
	}
	
}
