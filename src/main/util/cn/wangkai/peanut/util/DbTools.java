package cn.wangkai.peanut.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;


public class DbTools {
//	private static final Log log = LogFactory.getLog(DbTools.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

	public static boolean StrToBoolean(String str) {
		return BooleanUtils.toBoolean(str);
	}

	public static int StrToint(String intstr) {
		int returnint = 0;
		if(StringUtils.isBlank(intstr))return returnint;
		try {
			returnint = Integer.parseInt(intstr);
		} catch (Exception e) {
		}
		return returnint;
	}

	public static Integer StrToInteger(String intstr) {
		if(StringUtils.isBlank(intstr))return null;
		return Integer.valueOf(intstr);
	}
	
	public static Date StrToDate(String Datestr){
		if(StringUtils.isBlank(Datestr)) return null;
		try {
			return DateUtils.parseDate(Datestr, new String[] {"yyyy-MM-dd HH:mm:ss.SSS", "yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm","yyyy-MM-dd"});
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static String DateToStr(Date myDate) {
	    SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
	    String strDate = formatter.format(myDate);
	    return strDate;
	}
	
	public static java.sql.Date strtoSqlDate(String Datestr){
		if(StringUtils.isBlank(Datestr)) return null;
		try {
			Date date = DateUtils.parseDate(Datestr, new String[] {"yyyy-MM-dd HH:mm:ss.SSS", "yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm","yyyy-MM-dd"});
			return new java.sql.Date(date.getTime());
		} catch (ParseException e) {
			return null;
		}
	}
	public static java.sql.Date DateToSqlDate(long date){
		if(date==0) return null;
		return new java.sql.Date(date);
			//return new java.sql.Date(date.getTime());
	}
	
	public static java.sql.Date DateToSqlDate(Date date){
		if(date==null) return null;
		return DateToSqlDate(date.getTime());
	}
	
	public static Timestamp getNowTime(){
		return new Timestamp(new Date().getTime());
	}
//	public static boolean regIP(String ip){
//		String ipreg = "^((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]\\d|\\d)\\.){3}(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]\\d|[1-9])$";
//		return Domainutil.Match(ipreg, ip);
//	}
//	
//	public static boolean regURL(String url){
//		String urlreg = "http\\:\\/\\/(www\\.)?([\\w|-]+)\\.(\\w+)(\\.(\\w+))?(\\/)?$";
//		return Domainutil.Match(urlreg, url);
//	}
	
}
