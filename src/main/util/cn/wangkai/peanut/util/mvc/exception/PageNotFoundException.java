package cn.wangkai.peanut.util.mvc.exception;

public class PageNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PageNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public PageNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PageNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PageNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
