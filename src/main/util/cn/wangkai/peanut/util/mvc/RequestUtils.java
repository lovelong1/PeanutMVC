package cn.wangkai.peanut.util.mvc;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {

	public static String getRemoteAddr(HttpServletRequest request) {
		return request.getRemoteAddr();
	}

}
