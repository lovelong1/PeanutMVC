package cn.wangkai.peanut.util.mvc;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

public class ActionForward {

	public static String TYPE_VM="VM";
	public static String TYPE_JSP="JSP";
	
	private String uri = null;
	private String type = null;
	private Map<String, Object> root = new HashMap<String, Object>();
	
	public Map<String, Object> getRoot() {
		return root;
	}

	public void setRoot(Map<String, Object> root) {
		this.root = root;
	}

	public ActionForward(String uri) {
		this.uri = uri;
	}

	public ActionForward(String uri,String type) {
		this.uri = uri;
		this.type = type;
	}
	public ActionForward(String uri,String type,Map<String, Object> root) {
		this.uri = uri;
		this.type = type;
		this.root = root;
	}
	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public ActionForward() {
	}
	
	public static void main(String[] args) {
        URL resource = ActionForward.class.getResource("/vm/allpage/404.ftl");
        if(resource != null)
        {
            URLConnection urlConnection;
			try {
				urlConnection = resource.openConnection();
	            byte bytes[] = null;
	            java.io.InputStream is  = urlConnection.getInputStream();
	            bytes = IOUtils.toByteArray(is);
	            IOUtils.closeQuietly(is);
	            IOUtils.write(bytes, System.out);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}
	
}
