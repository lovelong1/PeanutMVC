package cn.wangkai.peanut.util.mvc;

import java.lang.reflect.Method;

public class UrlPattern {

	public static int DOTYPE_FORWARD=1;
	public static int DOTYPE_DEFAULT=0;
	private String url;
	private String action_method_name = "execute";
	private String claname = "index";
	private String pkgname = "";
	private Object action;
	private Method m_action;
	private String forward;
	private int dotype=DOTYPE_DEFAULT;
	
	public String getForward() {
		return forward;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public int getDotype() {
		return dotype;
	}

	public void setDotype(int dotype) {
		this.dotype = dotype;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAction_method_name() {
		return action_method_name;
	}

	public void setAction_method_name(String action_method_name) {
		this.action_method_name = action_method_name;
	}

	public String getClaname() {
		return claname;
	}

	public void setClaname(String claname) {
		this.claname = claname;
	}

	public String getPkgname() {
		return pkgname;
	}

	public void setPkgname(String pkgname) {
		this.pkgname = pkgname;
	}

	public Object getAction() {
		return action;
	}

	public void setAction(Object action) {
		this.action = action;
	}

	public Method getM_action() {
		return m_action;
	}

	public void setM_action(Method m_action) {
		this.m_action = m_action;
	}

	public UrlPattern() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
