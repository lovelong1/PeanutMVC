package cn.wangkai.peanut.util.servlet;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.beanutils.ConvertUtils;

import cn.wangkai.peanut.commons.beanutils.BigDecimalConverter;
import cn.wangkai.peanut.commons.beanutils.DateConverter;
import cn.wangkai.peanut.commons.beanutils.SetConverter;
import cn.wangkai.peanut.commons.beanutils.TimestampConverter;


public class InitServlet extends HttpServlet  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8280079087506699960L;

	public void init() throws ServletException {
		//注册日期类型的转换器 
	    ConvertUtils.register(new DateConverter(), Date.class);
	    ConvertUtils.register(new DateConverter(), java.sql.Date.class);
	    ConvertUtils.register(new TimestampConverter(), Timestamp.class);
	    ConvertUtils.register(new BigDecimalConverter(), BigDecimal.class);
	    ConvertUtils.register(new SetConverter(), Set.class);
	}
}
