package cn.wangkai.peanut.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class Configinfo {

	private static String config=null;
	
	/**
	 * 配置文件
	 */
	private static Properties p =null;
	
	private static void init(){
		InputStream inputStream = Configinfo.class.getClassLoader().getResourceAsStream("webserviceurl.properties");
		p = new Properties();
		try {
			p.load(inputStream);
			config = p.getProperty("config");
		} catch (IOException e) {
		}
	}

	public static String getConfig() {
		if(p==null){
			init();
		}
		return config;
	}

	public static void setConfig(String config) {
		Configinfo.config = config;
	}

	public Configinfo() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}
	public static String getconfig(String code,String defaultval){
		String str = getTemplates(code);
		if(StringUtils.isEmpty(str)) 
			return defaultval;
		else
			return str;
	}
	
	
	/**
	 * 得到模板数据
	 * @param SMSCODE 模板编号
	 * @return
	 */
	public static String getTemplates(String SMSCODE){
		if(p==null){
			init();
		}
		return p.getProperty(SMSCODE);
	}
	/**
	 * 得到模板数据
	 * @param SMSCODE 模板编号
	 * @param Value 替换字符
	 * @return
	 */
	public static String getTemplates(String SMSCODE,String Value){
		return getTemplates(SMSCODE, new String[]{Value});
	}
	
	/**
	 * 得到模板数据
	 * @param SMSCODE 模板编号
	 * @param Value 替换字符组
	 * @return
	 */
	public static String getTemplates(String SMSCODE,String[] Value){
		String temp = getTemplates(SMSCODE);
		if(StringUtils.isBlank(temp)) return null;
		if(Value==null) return temp;
		for (int i = 0; i < Value.length; i++) {
			String with = Value[i];
			temp = StringUtils.replace(temp, "${"+i+"}", with);
		}
		return temp;
		
	}
	
}
