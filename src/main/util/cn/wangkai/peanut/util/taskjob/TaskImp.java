package cn.wangkai.peanut.util.taskjob;


import java.util.Date;
import java.util.TimerTask;

import javax.servlet.ServletContext;

public class TaskImp extends TimerTask implements Task {


	public TaskImp(ServletContext context) {
	}
	public TaskImp() {
	}

	private ServletContext context;

	public void setContext(ServletContext context) {
		this.context = context;
	}

	private static boolean isRunning = true;
	public void run() {
		if (isRunning) {
			System.out.println(new Date());
		}
	}

	public ServletContext getContext() {
		return context;
	}

}
