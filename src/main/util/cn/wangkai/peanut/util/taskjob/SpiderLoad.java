package cn.wangkai.peanut.util.taskjob;

import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class SpiderLoad extends HttpServlet {

	private static final Log log = LogFactory.getLog(SpiderLoad.class);
	private static final long serialVersionUID = 1L;

	private Timer timer1 = null;

	public SpiderLoad() {
		log.debug("Load.....");
	}

	public void init() throws ServletException {
		log.debug("init");
//		log.debug(this.getServletContext().getContextPath());
		log.debug(this.getServletContext().getMajorVersion());
		log.debug(this.getServletContext().getMinorVersion());
		log.debug(this.getServletContext().getServerInfo());
		log.debug(this.getServletContext().getServletContextName());
//		ServletContext context = getServletContext();
		log.debug(getInitParameter("startTask"));
		log.debug(getInitParameter("intervalTime"));
		 String startTask = getInitParameter("startTask");
		 long delay = Long.parseLong(getInitParameter("intervalTime"));
		if (startTask.equals("true")) {
			timer1 = new Timer(true);
			DOTask task1=null;
			try {
				task1 = (DOTask) Class.forName("").newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			timer1.schedule(task1, delay * 2 * 1000, delay * 2 * 1000);
		}

	}

	public void destroy() {
		log.debug("destroy");
		super.destroy();
		if (timer1 != null) {
			timer1.cancel();
		}
	}

	public static void main(String[] args) {
	}
}
