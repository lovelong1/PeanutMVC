package cn.wangkai.peanut.util.taskjob;

import javax.servlet.ServletContext;

public interface Task{
	public void setContext(ServletContext context);
	public ServletContext getContext();
	public void run();
}
