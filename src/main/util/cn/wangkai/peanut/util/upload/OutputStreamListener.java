package cn.wangkai.peanut.util.upload;

public interface OutputStreamListener
{

    public abstract void start();

    public abstract void bytesRead(int i);

    public abstract void error(String s);

    public abstract void done();
}
