package cn.wangkai.peanut.util.upload;



//Referenced classes of package book.upload:
//         OutputStreamListener

public class UploadListener implements OutputStreamListener{
 public static class FileUploadStats{

     private long totalSize;
     private long bytesRead;
     private long startTime;
     private String currentStatus;

     public long getTotalSize()
     {
         return totalSize - 100L;
     }

     public void setTotalSize(long totalSize)
     {
         this.totalSize = totalSize;
     }

     public long getBytesRead()
     {
         return bytesRead;
     }

     public long getElapsedTimeInSeconds()
     {
         return (System.currentTimeMillis() - startTime) / 1000L;
     }

     public String getCurrentStatus()
     {
         return currentStatus;
     }

     public void setCurrentStatus(String currentStatus)
     {
         this.currentStatus = currentStatus;
     }

     public void setBytesRead(long bytesRead)
     {
         this.bytesRead = bytesRead;
     }

     public void incrementBytesRead(int byteCount)
     {
         bytesRead += byteCount;
     }

     public FileUploadStats()
     {
         totalSize = 0L;
         bytesRead = 0L;
         startTime = System.currentTimeMillis();
         currentStatus = "none";
     }
 }


 private FileUploadStats fileUploadStats;

 public UploadListener(long totalSize)
 {
     fileUploadStats = new FileUploadStats();
     fileUploadStats.setTotalSize(totalSize);
 }

 public void start()
 {
     fileUploadStats.setCurrentStatus("start");
 }

 public void bytesRead(int byteCount)
 {
     fileUploadStats.incrementBytesRead(byteCount);
     fileUploadStats.setCurrentStatus("reading");
 }

 public void error(String s)
 {
     fileUploadStats.setCurrentStatus("error");
 }

 public void done()
 {
     fileUploadStats.setBytesRead(fileUploadStats.getTotalSize());
     fileUploadStats.setCurrentStatus("done");
 }

 public FileUploadStats getFileUploadStats()
 {
     return fileUploadStats;
 }
}