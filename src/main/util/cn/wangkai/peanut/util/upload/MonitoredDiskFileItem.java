package cn.wangkai.peanut.util.upload;

import java.io.*;
import org.apache.commons.fileupload.disk.DiskFileItem;

// Referenced classes of package book.upload:
//            MonitoredOutputStream, OutputStreamListener

public class MonitoredDiskFileItem extends DiskFileItem{
	private static final long serialVersionUID = 1L;
	private MonitoredOutputStream mos;
    private OutputStreamListener listener;

    public MonitoredDiskFileItem(String fieldName, String contentType, boolean isFormField, String fileName, int sizeThreshold, File repository, OutputStreamListener listener){
        super(fieldName, contentType, isFormField, fileName, sizeThreshold, repository);
        mos = null;
        this.listener = listener;
    }

    public OutputStream getOutputStream()
        throws IOException{
        if(mos == null){
            mos = new MonitoredOutputStream(super.getOutputStream(), listener);
        }
        return mos;
    }
}
