package cn.wangkai.peanut.util.upload;

import java.io.File;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

// Referenced classes of package book.upload:
//            MonitoredDiskFileItem, OutputStreamListener

public class MonitoredDiskFileItemFactory extends DiskFileItemFactory{

    private OutputStreamListener listener;

    public MonitoredDiskFileItemFactory(OutputStreamListener listener){
        this.listener = null;
        this.listener = listener;
    }

    public MonitoredDiskFileItemFactory(int sizeThreshold, File repository, OutputStreamListener listener){
        super(sizeThreshold, repository);
        this.listener = null;
        this.listener = listener;
    }

    public FileItem createItem(String fieldName, String contentType, boolean isFormField, String fileName){
        return new MonitoredDiskFileItem(fieldName, contentType, isFormField, fileName, getSizeThreshold(), getRepository(), listener);
    }
}
