package cn.wangkai.peanut.util.freemarker;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.wangkai.peanut.db.MyDb;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.system.dao.sequence.CodetableManager;
import cn.wangkai.peanut.system.dao.sequence.CodetableManagerImp;
import cn.wangkai.peanut.system.db.PeanutDb;
import cn.wangkai.peanut.system.model.Codetable;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class Mb implements TemplateMethodModelEx {
	private Log log = LogFactory.getLog(Mb.class);
	private CodetableManager codetableManager = new CodetableManagerImp();
	private PeanutDb db = PeanutDb.Open();
	private Codetable getCodeTable(String code,String pcode,String category){
		Codetable codetable = null;
		try {
			codetable = codetableManager.findCacheByCode(code, null, pcode, category, null);
		} catch (Exception e) {
//			e.printStackTrace();
		}

		if(codetable!=null) return codetable;
		IMyDb myDb = MyDb.Open(db);
		try {
			return codetableManager.findByCode(myDb, code,null, pcode, category,null);
		} catch (Exception e) {
			return null;
		} finally {
			myDb.Close();
		}
	}
//	private Unit getUnit(String dwxh){
//		if(StringUtils.isEmpty(dwxh)) return null;
//		Unit unit = null;
//		try {
//			if(StringUtils.isEmpty(dwxh)) return null;
//			unitManager.findBydwbh(db, dwbh)
//			unit = codetableManager.findUnitBydwbh(dwxh);
//		} catch (Exception e) {}
//		
//		if(unit!=null) return unit;
//		IMyDb myDb = MyDb.Open(db);
//		try {
//			return codetableManager.findUnitBydwbh(myDb, dwxh);
//		} catch (Exception e) {
//			return null;
//		} finally {
//			myDb.Close();
//		}
//	}
	
	@SuppressWarnings("rawtypes")
	public Object exec(List args) throws TemplateModelException {
		log.debug(args);
		if(args==null||args.size()<2) return null;
		for (Object object : args) {
			log.debug(object);
		}
		String type = null;
		String category = null;
		String code = null;
		String pcode = null;
		String show = null;
		if(args.size()==2){
			type = "code";
			category = args.get(0).toString();
			code = args.get(1).toString();
		}else{
			type = args.get(0).toString();
			category = args.get(1).toString();
			if(args.get(2)!=null) code = args.get(2).toString();
		}
		if(args.size()>3) pcode = args.get(3).toString();
		if(args.size()>4) pcode = args.get(4).toString();
		if(code==null) return "";
		String[] codes = StringUtils.split(code, ",");
		String returnstr = "";
		for (String codestr : codes) {
			if(StringUtils.isNotEmpty(returnstr)) returnstr += ",";
			if(type==null||StringUtils.equalsIgnoreCase(type, "code")){
					Codetable codetable = getCodeTable(codestr,pcode,category);
					if(codetable==null){
						returnstr += codestr;
					}else if(StringUtils.isEmpty(show)||StringUtils.equalsIgnoreCase("name", show)){
						returnstr += codetable.getName();
					}else if(StringUtils.equalsIgnoreCase("showparent", show)){
						String pnamestr = codetable.getName();
						while(codetable!=null&&codetable.getPcode()!=null){
							codetable = getCodeTable(codetable.getPcode(), null, category);
							if(StringUtils.isNotEmpty(pnamestr)){
								pnamestr = codetable.getName()+"&gt;&gt;"+pnamestr;
							}
						}
						returnstr += pnamestr;
					}else if(StringUtils.equalsIgnoreCase("describe", show)){
						returnstr += codetable.getDescribe();
					}else if(StringUtils.equalsIgnoreCase("pcode", show)){
						returnstr += codetable.getPcode();
					}else{
						returnstr += codetable.getName();
					}
			}else if(StringUtils.equalsIgnoreCase(type, "unit")){
//				Unit unit = getUnit(codestr);
//				if(unit==null){
//					returnstr += codestr;
//				}else if(StringUtils.isEmpty(show)||StringUtils.equalsIgnoreCase("name", show)){
//					returnstr += unit.getDwmc();
//				}else{
//					returnstr += unit.getDwmc();
//				}
			}
		}
		return returnstr;
	}
	
	
	public static void main(String[] args) {
		System.out.println(StringUtils.split("asdfasdfasdf",",").length);
		System.out.println(StringUtils.split("asdfasdfsadf,",",").length);
		System.out.println(StringUtils.split("sadfsadfsadf,sdfsdf,sfsdfsdf",",").length);
		System.out.println(StringUtils.split(",sdfsdf",",").length);
	}

}