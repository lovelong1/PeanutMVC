package cn.wangkai.peanut.util.freemarker;

import java.util.List;
import java.util.Map;

import cn.wangkai.peanut.mvc.admin.model.Right;
import cn.wangkai.peanut.util.RequestContext;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class Cr implements TemplateMethodModelEx {
	@SuppressWarnings("rawtypes")
	public Object exec(List list) throws TemplateModelException {
		RequestContext context = RequestContext.get();
		if(context==null||context.getUser()==null||(context.getUser().getRights().size()<=0)) return "false";
		Map<String,Right> rights = context.getUser().getQrights();
		for (Object object : list) {
			if(rights.get(object+"")!=null) return "true";
		}
		return "false";
	}

}


