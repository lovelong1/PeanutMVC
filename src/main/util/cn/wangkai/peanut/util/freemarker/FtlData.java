package cn.wangkai.peanut.util.freemarker;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.ocpsoft.prettytime.PrettyTime;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class FtlData implements TemplateMethodModelEx {

	@SuppressWarnings("rawtypes")
	public Object exec(List args) throws TemplateModelException {
		String datePattern = (args.get(0).toString());
		if(datePattern==null) return "未知";
		Date date = null;
		try{
			date = DateUtils.parseDate(datePattern, new String[] {"yyyy-MM-dd HH:mm:ss.SSS", "yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm","yyyy-MM-dd"});
		}catch (Exception e) {
		}
		if(date==null) return datePattern;
		PrettyTime p = new PrettyTime();
		return p.format(date);
	}

}