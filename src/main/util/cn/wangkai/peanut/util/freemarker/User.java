package cn.wangkai.peanut.util.freemarker;

import java.sql.SQLException;
import java.util.List;

import cn.wangkai.peanut.db.MyDb;
import cn.wangkai.peanut.db.WKDbException;
import cn.wangkai.peanut.db.iface.IMyDb;
import cn.wangkai.peanut.mvc.admin.dao.manager.UserManager;
import cn.wangkai.peanut.mvc.admin.dao.manager.UserManagerImp;
import cn.wangkai.peanut.system.db.PeanutDb;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
/**
 * 用户信息
 * @author Administrator
 *
 */
public class User implements TemplateMethodModelEx {
	
	private UserManager userManager = new UserManagerImp();
	private PeanutDb db = PeanutDb.Open();
	
	@SuppressWarnings("rawtypes")
	public Object exec(List list) throws TemplateModelException {
		String userid = (list.get(0).toString());
		IMyDb myDb = MyDb.Open(db);
		try {
			return  userManager.findUserByUserID(myDb,userid);
		} catch (SQLException e) {
		} catch (WKDbException e) {
		}finally{
			myDb.Close();
		}
		return userid;
	}

}


