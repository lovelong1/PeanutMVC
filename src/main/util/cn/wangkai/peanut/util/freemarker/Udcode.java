package cn.wangkai.peanut.util.freemarker;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class Udcode implements TemplateMethodModelEx {
	@SuppressWarnings("rawtypes")
	public Object exec(List list) throws TemplateModelException {
		if(list==null||list.size()>2) return null;
		String str = null;
		String encode = null;
		str = list.get(0).toString();
		if(list.size()==2){
			encode = list.get(1).toString();
		}else{
			encode = "UTF-8";
		}
		try {
			return URLEncoder.encode(str, encode);
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}

	public static void main(String[] args) {
		try {
			System.out.println(URLEncoder.encode("凤凰", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


