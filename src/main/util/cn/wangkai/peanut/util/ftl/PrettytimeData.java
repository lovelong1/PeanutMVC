package cn.wangkai.peanut.util.ftl;

import java.util.Date;
import java.util.List;

import org.ocpsoft.prettytime.PrettyTime;

import cn.wangkai.peanut.util.DbTools;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class PrettytimeData implements TemplateMethodModelEx {

	@SuppressWarnings("rawtypes")
	public Object exec(List args) throws TemplateModelException {
		// 得到函数第一个参数,得到的字符串两头会有引号,所以replace
		String datePattern = (args.get(0).toString());
		if(datePattern==null) return "未知";
		//2014-04-23 17:02:56720
		if(datePattern.length()>18) datePattern =datePattern.substring(0,19)+"."+datePattern.substring(19);
		Date date = DbTools.StrToDate(datePattern);
		PrettyTime p = new PrettyTime();
		return p.format(date);
	}

}