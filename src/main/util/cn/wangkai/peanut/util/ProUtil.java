package cn.wangkai.peanut.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProUtil {
	//日志系统
	private static Logger logger = Logger.getAnonymousLogger();

	//默认文件名
	private static String propertiesfilename = "ini.properties";

	//文件路径
	private static String profilepath;
	
	//配置文件
	private static Properties prop;
	
	//写入描述
	private static String  comments = ProUtil.class.getName()+" write file";
	
	static{
		try {
			//profilepath  = URLDecoder.decode(ProUtil.class.getClassLoader().getResource("").getFile()+propertiesfilename,"UTF-8");
			profilepath  = URLDecoder.decode(ProUtil.class.getClassLoader().getResource("").getFile().replaceAll("WEB-INF/classes/", "")+propertiesfilename,"UTF-8");
			File file = new File(profilepath);
			logger.log(Level.INFO,"读取配置文件"+file.getPath());
			if (!file.exists()) {
				try {
					logger.log(Level.INFO, (file.getPath()+"文件不存在，创建！"));
					 file.createNewFile();
				} catch (IOException e) {
					logger.log(Level.WARNING, "出现错误！", e);
					e.printStackTrace();
				}
			}
			readini();
		} catch (Exception e) {
			logger.log(Level.WARNING, "出现错误！", e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 读取配置文件
	 */
	private static void readini(){
		try {
			if(prop == null) prop = new Properties();
			logger.info(profilepath);
			InputStream in = new FileInputStream(profilepath);
			prop.load(in);
		} catch (IOException e) {
			logger.log(Level.WARNING, "出现错误！", e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 检查某一个键值是否存在
	 * @param key 键值
	 * @return 存在返回true，不存在返回false
	 */
	private static boolean haskey(String key){
		if(prop == null) readini();
		return prop.containsKey(key);
	}
	/**
	 * 根据键值读取某一个配置
	 * @param key 键值
	 * @return 键值存在，读取键值数值，不存在返回默认值
	 */
	public static String read(String key){
		return read(key, null);
	}
	
	/**
	 * 根据键值读取某一个配置
	 * @param key 键值
	 * @param defalutval 键值默认值
	 * @return 键值存在，读取键值数值，不存在返回默认值
	 */
	public static String read(String key,String defalutval){
		return read(key, defalutval, false);
	}
	
	/**
	 * 根据键值读取某一个配置
	 * @param key 键值
	 * @param defalutval 键值默认值
	 * @param cwrite 不存在是否写入默认值
	 * @return 键值存在，读取键值数值，不存在返回默认值
	 */
	public static String read(String key,String defalutval,boolean cwrite){
		if(prop == null) readini();
		if((!haskey(key))&&cwrite){
			logger.log(Level.WARNING, key+"不存在！创建");
			write(key, defalutval);
		}
		return prop.getProperty(key, defalutval);
	}
	
	/**
	 * 写入某一个键和值
	 * @param key 键
	 * @param value 值
	 */
	public static void write(String key,String value){
		FileOutputStream oFile = null;
		try {
			if(prop == null) readini();
			oFile = new FileOutputStream(profilepath, false);
			if(!haskey(key)){
				prop.put(key, value);
			}else{
				prop.replace(key, value);
			}
			prop.store(oFile,comments);
		} catch (IOException e) {
			logger.log(Level.WARNING, "出现错误！", e);
			e.printStackTrace();
		}finally {
			try {
				if(oFile!=null) oFile.close();
			} catch (IOException e) {}
		}
		readini();
	}
	
	public static void main(String[] args) {
		logger.setLevel(Level.INFO);
		logger.info(ProUtil.read("abc", "456663",true));//读取一个配置文件,不存在写入，返回默认值
		logger.info(ProUtil.read("bbb", "123",false));//读取一个配置文件，不存在，直接返回默认值
		ProUtil.write("sk", System.currentTimeMillis()+"");//直接写入一个配置文件
		ProUtil.write("sk2", "写入中文");//写入中文测试
		logger.info(ProUtil.read("sk2", null, false));//读取中文
	}
}
